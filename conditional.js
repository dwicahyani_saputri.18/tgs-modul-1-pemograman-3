// TGS CONDITIONAL //
// DWI CAHYANI SAPUTRI 2018010009//
const nama = 'Jane';
const peran = 'Penyihir';

if (nama == '' && peran == '') {
    console.log('Nama Harus Diisi!');
} else if (nama == 'John' && peran == ''){
    console.log('Halo John, Pilih peranmu untuk memulai game');
} else if (nama == 'Jane' && peran == 'Penyihir') {
    console.log('Selamat datang di Dunia Werewolf, '+ nama);
    console.log('Halo '+ peran + nama +', kamu dapat melihat siapa yang menjadi werewolf!');
} else if (nama == 'Jenita' && peran == 'Guard') {
    console.log('Selamat datang di Dunia Werewolf, '+ nama);
    console.log('Halo '+ peran + nama +', kamu akan membantu melindungi temanmu dari serangan werewolf');
} else if (nama == 'Junaedi' && peran == 'Werewolf') {
    console.log('Selamat datang di Dunia Werewolf, '+ nama);
    console.log('Halo '+ peran + nama +', kamu akan memakan mangsa setiap malam!');
} else {
    console.log('Server Maintance!');
}
console.log('====================================');

const tanggal = 21;
let bulan = 1;
const tahun = 1945;

switch(bulan) {
    case 1:
        bulan = "Januari";
    break;
    case 2:
        bulan = "Februari";
    break;
    case 3:
        bulan = "Maret";
    break;
    case 4:
        bulan = "April";
    break;
    case 5:
        bulan = "Mei";
    break;
    case 6:
        bulan = "Juni";
    break;
    case 7:
        bulan = "Juli";
    break;
    case 8:
        bulan = "Agustus";
    break;
    case 9:
        bulan = "September";
    break;
    case 10:
        bulan = "Oktober";
    break;
    case 11:
        bulan = "November";
    break;
    case 12:
        bulan = "Desember";
    break;
}

const tampilTanggal = 'Tanggal: ' + tanggal + ' ' + bulan + ' ' + tahun;
console.log(tampilTanggal);
